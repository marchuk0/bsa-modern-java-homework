package com.binary_studio.tree_max_depth;

import java.util.Iterator;
import java.util.Stack;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		return nonRecursiveDepthFirstSearch(rootDepartment);
	}

	public static Integer depthFirstSearch(Department department) {
		if (department == null) {
			return 0;
		}
		int maxSubDepth = 0;
		Iterator iterator = department.subDepartments.iterator();

		while (iterator.hasNext()) {
			maxSubDepth = Math.max(maxSubDepth, depthFirstSearch((Department) iterator.next()));
		}
		return maxSubDepth + 1;
	}

	public static Integer nonRecursiveDepthFirstSearch(Department rootDepartment) {
		Stack<Department> stack = new Stack<>();
		stack.push(rootDepartment);

		Department department;
		Iterator<Department> iterator;
		while (!stack.isEmpty()) {
			if (stack.peek() == null) {
				stack.pop();
			}
			else if (stack.peek().isVisited()) {
				department = stack.pop();
				iterator = department.subDepartments.iterator();
				int newDepth = 1;
				while (iterator.hasNext()) {
					var childDepartment = iterator.next();
					if (childDepartment != null) {
						newDepth = Math.max(newDepth, childDepartment.getDepth() + 1);
					}
				}
				department.setDepth(newDepth);
			}
			else {
				department = stack.peek();
				iterator = department.subDepartments.iterator();
				while (iterator.hasNext()) {
					stack.push(iterator.next());
				}
				department.setVisited(true);
			}
		}

		if (rootDepartment == null) {
			return 0;
		}
		else {
			return rootDepartment.getDepth();
		}
	}

}
