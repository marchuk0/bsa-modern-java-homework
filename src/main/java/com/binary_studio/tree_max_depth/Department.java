package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Department {

	public final String name;

	public final List<Department> subDepartments;

	private int depth;

	private boolean visited = false;

	public Department(String name) {
		this.name = name;
		this.subDepartments = new ArrayList<>();
	}

	public Department(String name, Department... departments) {
		this.name = name;
		this.subDepartments = Arrays.asList(departments);
	}

	public int getDepth() {
		return this.depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public boolean isVisited() {
		return this.visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

}
