package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public class SubsystemImpl implements Subsystem {

	private final String name;

	private final PositiveInteger powerGridConsumption;

	private final PositiveInteger capacitorConsumption;

	public SubsystemImpl(String name, PositiveInteger powerGridConsumption, PositiveInteger capacitorConsumption)
			throws IllegalArgumentException {
		if (name == null || name.replaceAll("\\s", "").isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.powerGridConsumption = powerGridConsumption;
		this.capacitorConsumption = capacitorConsumption;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
