package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl extends SubsystemImpl implements AttackSubsystem {

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		super(name, powergridConsumption, capacitorConsumption);
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = (target.getSize().value() >= this.optimalSize.value()) ? 1.0
				: target.getSize().value() / (double) this.optimalSize.value();
		double speedReductionModifier = (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) ? 1.0
				: this.optimalSpeed.value() / (2.0 * target.getCurrentSpeed().value());
		int damage = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of(damage);

	}

}
