package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final ShipProperties shipProperties;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	private PositiveInteger currentCapacitorAmount;

	public CombatReadyShip(ShipProperties shipProperties, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.shipProperties = shipProperties;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.currentCapacitorAmount = shipProperties.getCapacitorAmount();
		this.currentHullHP = shipProperties.getHullHP();
		this.currentShieldHP = shipProperties.getShieldHP();
	}

	@Override
	public void endTurn() {
		int maxAmount = this.shipProperties.getCapacitorAmount().value();
		int rechargeAmount = this.shipProperties.getCapacitorRechargeAmount().value();
		int currentAmount = this.currentCapacitorAmount.value();
		this.currentCapacitorAmount = PositiveInteger.of(Math.min(currentAmount + rechargeAmount, maxAmount));
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.shipProperties.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.shipProperties.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.shipProperties.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.currentCapacitorAmount.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		attack = this.defenciveSubsystem.reduceDamage(attack);
		int damage = attack.damage.value();
		if (damage > this.currentHullHP.value() + this.currentShieldHP.value()) {
			return new AttackResult.Destroyed();
		}
		if (this.currentShieldHP.value() < damage) {
			damage -= this.currentShieldHP.value();
			this.currentShieldHP = PositiveInteger.of(0);
		}
		else {
			this.currentShieldHP = PositiveInteger.of(this.currentShieldHP.value() - damage);
			damage = 0;
		}
		if (this.currentHullHP.value() < damage) {
			damage -= this.currentHullHP.value();
			this.currentHullHP = PositiveInteger.of(0);
		}
		else {
			this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() - damage);
		}
		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(attack.damage.value() - damage),
				attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.currentCapacitorAmount.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());

		var regenerated = this.defenciveSubsystem.regenerate();
		int shieldHPRegenerated = Math.min(this.shipProperties.getShieldHP().value() - this.currentShieldHP.value(),
				regenerated.shieldHPRegenerated.value());
		int hullHPRegenerated = Math.min(this.shipProperties.getHullHP().value() - this.currentHullHP.value(),
				regenerated.hullHPRegenerated.value());
		var newRegenerated = new RegenerateAction(PositiveInteger.of(shieldHPRegenerated),
				PositiveInteger.of(hullHPRegenerated));

		return Optional.of(newRegenerated);
	}

}
