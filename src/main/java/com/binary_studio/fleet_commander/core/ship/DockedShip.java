package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final ShipProperties shipProperties;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(new ShipProperties(name, shieldHP, hullHP, powerGridOutput, capacitorAmount,
				capacitorRechargeRate, speed, size));
	}

	public DockedShip(ShipProperties shipProperties) {
		this.shipProperties = shipProperties;
		this.attackSubsystem = null;
		this.defenciveSubsystem = null;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		this.attackSubsystem = subsystem;
		int delta = this.checkSubsystems();
		if (delta < 0) {
			throw new InsufficientPowergridException(-delta);
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		this.defenciveSubsystem = subsystem;
		int delta = this.checkSubsystems();
		if (delta < 0) {
			throw new InsufficientPowergridException(-delta);
		}
	}

	private int checkSubsystems() {
		return this.shipProperties.getPowerGridOutput().value()
				- (this.defenciveSubsystem == null ? 0 : this.defenciveSubsystem.getPowerGridConsumption().value())
				- (this.attackSubsystem == null ? 0 : this.attackSubsystem.getPowerGridConsumption().value());

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		return new CombatReadyShip(this.shipProperties, this.attackSubsystem, this.defenciveSubsystem);
	}

}
